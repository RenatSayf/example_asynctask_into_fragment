package com.example.renat.example_asynctask_into_fragment;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;


// TODO: 1.7 Активити реалезует интерфейс AsyncTaskFragment.OnFragmentInteractionListener
// TODO: 2.7 Активити реалезует интерфейс AsyncTaskFragment2.OnFragmentInteractionListener
public class MainActivity extends AppCompatActivity implements AsyncTaskFragment.OnFragmentInteractionListener,
        AsyncTaskFragment2.OnFragmentInteractionListener
{
    private TextView textView;
    private ProgressBar progressBar;

    // TODO: 1.9 объявление фрагмента
    private AsyncTaskFragment asyncTaskFragment;

    // TODO: 2.9 объявление фрагмента
    private AsyncTaskFragment2 asyncTaskFragment2;
    private FragmentManager fragmentManager;

    private String KEY_TEXT = "key_text";
    private String KEY_PROGRESS = "key_progress";

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_TEXT, textView.getText().toString());
        outState.putInt(KEY_PROGRESS, progressBar.getVisibility());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentManager = getSupportFragmentManager();

        textView = (TextView) findViewById(R.id.textView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);

        Button buttonRun = (Button) findViewById(R.id.btn_run);
        buttonRun.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                textView.setText(R.string.text_task_is_runs);
                progressBar.setVisibility(View.VISIBLE);

                // TODO: 1.10 Запуск асинхронной задачи во фрагменте
                asyncTaskFragment = (AsyncTaskFragment) fragmentManager.findFragmentByTag("data");
                if (asyncTaskFragment != null)
                {
                    fragmentManager.beginTransaction().remove(asyncTaskFragment).commit();
                }
                asyncTaskFragment = new AsyncTaskFragment();
                fragmentManager.beginTransaction().add(asyncTaskFragment, "data").commit();
            }
        });

        Button buttonRun2 = (Button) findViewById(R.id.btn_run2);
        buttonRun2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                textView.setText(R.string.text_task_is_runs);
                progressBar.setVisibility(View.VISIBLE);

                // TODO: 2.10 Запуск асинхронной задачи во фрагменте
                asyncTaskFragment2 = (AsyncTaskFragment2) fragmentManager.findFragmentByTag("data2");
                if (asyncTaskFragment2 != null)
                {
                    fragmentManager.beginTransaction().remove(asyncTaskFragment2).commit();
                }
                asyncTaskFragment2 = AsyncTaskFragment2.newInstance("AAAAA", "SSSSS");
                fragmentManager.beginTransaction().add(asyncTaskFragment2, "data2").commit();
            }
        });

        if (savedInstanceState != null)
        {
            textView.setText(savedInstanceState.getString(KEY_TEXT));
            progressBar.setVisibility(savedInstanceState.getInt(KEY_PROGRESS));
        }
    }


    // TODO: 1.8 Реализация интерфейса AsyncTaskFragment.OnFragmentInteractionListener
    @Override
    public void onFragmentInteraction(Object object)
    {
        // в переменной object содержится результат асинхронной задачи
        textView.setText(object.toString());
        progressBar.setVisibility(View.INVISIBLE);
    }

    // TODO: 2.8 Реализация интерфейса AsyncTaskFragment2.OnFragmentInteractionListener
    @Override
    public void onFragmentInteraction2(Object object)
    {
        // в переменной object содержится результат асинхронной задачи
        textView.setText(object.toString());
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }
}
