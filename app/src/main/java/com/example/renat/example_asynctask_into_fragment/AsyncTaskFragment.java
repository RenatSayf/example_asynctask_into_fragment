package com.example.renat.example_asynctask_into_fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import java.util.concurrent.TimeUnit;

// TODO: 1.1 Средствами Android Studio добавить новый фрагмент без макета, без factrory method но с interface collback
public class AsyncTaskFragment extends Fragment
{
    private OnFragmentInteractionListener mListener;

    public AsyncTaskFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);    // TODO: 1.2 true - что бы фрагмент не пересоздавался при изменении конфигурации

        // TODO: 1.3 Создание асинхронной задачи
        AsyncTask<Void, Void, String> asyncTask = new AsyncTask<Void, Void, String>()
        {
            @Override
            protected String doInBackground(Void... voids)
            {
                try
                {
                    TimeUnit.SECONDS.sleep(5);
                } catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
                return "Задача 1 завершена!!!";
            }

            @Override
            protected void onPostExecute(String s)
            {
                super.onPostExecute(s);
                // TODO: 1.5 Определение обратного вызова после завершения задачи
                if (mListener != null)
                {
                    mListener.onFragmentInteraction(s);
                }
            }
        };
        // TODO: 1.4 Запуск асинхронной задачи
        if (asyncTask.getStatus() != AsyncTask.Status.RUNNING)
        {
            asyncTask.execute();
        }
    }

    public void onAttach(Context context)   // этод метод созданный Android Studio оставить без изменения
    {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) context;
        } else
        {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach()  // этод метод созданный Android Studio оставить без изменения
    {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    // TODO: 1.6 Интерфейс созданный Android Studio - изменить тип параметра на Object
    public interface OnFragmentInteractionListener
    {
        void onFragmentInteraction(Object object);
    }
}
