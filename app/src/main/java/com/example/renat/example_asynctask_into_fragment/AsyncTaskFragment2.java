package com.example.renat.example_asynctask_into_fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import java.util.concurrent.TimeUnit;

// TODO: 2.1 Средствами Android Studio добавить новый фрагмент без макета, но с factrory method и interface collback
public class AsyncTaskFragment2 extends Fragment
{
    // Ключи для параметров
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // Типы и имена параметров
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public AsyncTaskFragment2()
    {
        // Required empty public constructor
    }

    // factrory method для создания эжкземпляра AsyncTaskFragment2 (созданный Android Studio)
    public static AsyncTaskFragment2 newInstance(String param1, String param2)
    {
        AsyncTaskFragment2 fragment = new AsyncTaskFragment2();
        // передача параметров в onCreate()
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setRetainInstance(true); // TODO: 2.2 true - что бы фрагмент не пересоздавался при изменении конфигурации

        if (getArguments() != null)
        {
            // получение параметров
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

            // TODO: 2.3 Создание асинхронной задачи
            AsyncTask<Void, Void, String> asyncTask = new AsyncTask<Void, Void, String>()
            {
                @Override
                protected String doInBackground(Void... voids)
                {
                    try
                    {
                        TimeUnit.SECONDS.sleep(5); // имитация долгой и тяжелой работы
                    } catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                    return "Задача 2 с параметрами: " + mParam1 + " и " + mParam2 + " - завершена!!!";
                }

                @Override
                protected void onPostExecute(String s)
                {
                    super.onPostExecute(s);
                    // TODO: 2.5 Определение обратного вызова после завершения задачи
                    if (mListener != null)
                    {
                        mListener.onFragmentInteraction2(s);
                    }
                }
            };
            // TODO: 2.4 Запуск асинхронной задачи
            if (asyncTask.getStatus() != AsyncTask.Status.RUNNING)
            {
                asyncTask.execute();
            }
        }
    }

    @Override
    public void onAttach(Context context) // этод метод созданный Android Studio оставить без изменения
    {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) context;
        } else
        {
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() // этод метод созданный Android Studio оставить без изменения
    {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    // TODO: 2.6 Интерфейс созданный Android Studio - изменить тип параметра на Object
    public interface OnFragmentInteractionListener
    {
        void onFragmentInteraction2(Object object);
    }
}
